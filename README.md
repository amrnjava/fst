**Data**


- RNA reads (125b), from Huylmans et. al, 2019

    - *A.sinica* 
        - pool of 10 males: head, thorax, testes

        - pool of 10 females: head, thorax, ovaries

    - *A.franciscana* 
        - pool of 10 males: head, testes

        - pool of 10 females: head, ovaries
    

- reference genomes
    - *A.sinica* male (ZZ), in-house assembly
    - *A.franciscana* male (ZZ), KPI



**Pipeline**

- [ ] map male and female RNA reads to the reference genome
- [ ] calculate Fst between males and females

**Detailed pipeline**
* *adapted from https://sourceforge.net/p/popoolation2/wiki/Tutorial/*

0. Trimming reads with Trimmomatic 0.36 (http://www.usadellab.org/cms/index.php?page=trimmomatic)
    +    `java -jar PATH/Trimmomatic-0.36/trimmomatic-0.36.jar PE -phred33 fran_female.1.fastq fran_female.2.fastq  fran_female.1_paired.fastq fran_female.1_unpaired.fastq fran_female.2_paired.fastq fran_female.2_unpaired.fastq ILLUMINACLIP:PATH/Trimmomatic-0.36/adapters/TruSeq3-PE.fa:2:30:10 LEADING:3 TRAILING:3 SLIDINGWINDOW:4:15 MINLEN:36`


1. Merge reads
    + male and female forward and reverse reads from different tissues were separately merged 
    + example:
        + `cat ~/fst2/reads/kazak/male/*.1.fastq > kazak_male.1.fastq`
    + catfastq.sh
2. Map reads

To map the reads, we can use STAR, aligner designed for RNA-seq libraries, enabling spliced alignments.

 https://github.com/alexdobin/STAR/blob/master/doc/STARmanual.pdf

+ Indexing the reference genome
    + star_index.sh
```
module load star/2.6.0c
srun STAR --runThreadN 20 --runMode genomeGenerate --genomeDir ~/fst2/ref_new --genomeFastaFiles ~/Fst/ref_genome/AsinicaMpbPilon_flye.fasta

```

+ Mapping the reads
    + star_map_generic.sh
```
srun STAR --runThreadN 20 --genomeDir ~/fst2/ref_new --readFilesIn FILENAME.1.fastq FILENAME.2.fastq  --outFileNamePrefix ~/fst2/map_newref/FILENAME.
```


- get MAPQ scores: mapq.sh

3. Filtering and sorting alignments 
    + sort_generic.sh
* filtering alignments with MAPQ score < 20 `samtools view -q 20`  
         http://www.htslib.org/doc/samtools-view.html

* sort alignments by leftmost coordinates `samtools sort`
         http://www.htslib.org/doc/samtools-sort.html

```
module load samtools

samtools view -q 20 -bS FILENAME | samtools sort -o ~/fst2/map/FILENAME.sort
```
4. Piling up male and female reads
* samtools mpileup takes sorted alignment files as an input and produces a pileup file, where each line corresponds to pileup of reads from two (or more) alignments at a single genomic position
    + http://www.htslib.org/doc/samtools-mpileup.html
```
samtools mpileup -B sinica_female.Aligned.out.sam.sort sinica_male.Aligned.out.sam.sort > sinica_mf.mpileup
```
+ 
    + mpileup_mf_sinica.sh


5. Creating PoPoolation2 synchronized files
* a synchronized file contains the allele frequencies for every population at every base in the reference genome (after filtering for base quality)

- synchronized file format:
    - col1: reference contig
    - col2: position within the reference contig
    - col3: reference character
    - col4: allele frequencies of population number 1
    - col5: allele frequencies of population number 2
    - coln: allele frequencies of population number n
        - allele frequencies are in the format A:T:C:G:N:del

* We synchronize the mpileup file with the script from PoPoolation2
```
perl ~/popoolation2-code/mpileup2sync.pl --input FILENAME --output FILENAME.sync --fastq-type sanger --min-qual 20
```

-  
    - pop_sync_generic.sh

6. Calculating Fst for 1000nt windows 
* after we obtained synchronized files, we can use them as the input for PoPoolation2 script that calculates Fst
```
perl ~/popoolation2-code/fst-sliding.pl --input FILENAME --output FILENAME.fst --suppress-noninformative --min-count 3 --min-coverage 10 --max-coverage 200 --min-covered-fraction 0.5 --window-size 1000 --step-size 1000 --pool-size 10
```
- options:
     - --input
        - The input file. Has to be synchronized pileup file. Mandatory parameter
    - --output
        - The output file. Mandatory parameter
 
    - --min-count
        - the minimum count of the minor allele; used for SNP identification.
        SNPs will be identified considering all populations simultanously.

    - --min-coverage
        - the minimum coverage; used for SNP identification, the coverage in ALL
        populations has to be higher or equal to this threshold, otherwise no
        SNP will be called.

    - --max-coverage
        - The maximum coverage; All populations are required to have coverages lower or equal
         than the   maximum coverage
    - --min-covered-fraction
        - the minimum fraction of a window being between min-coverage and
        max-coverage in ALL populations; 
    - --window-size
        - the size of the sliding window; default=1000

    - --step-size
        - the size of the sliding window steps; default=1000

    - --pool-size
        - the size of the population pools; May be provided for each population
        individually; mandatory parameter

    - --suppress-noninformative
        - Suppress output for windows with no SNPs or insufficient coverage;

* The output has a line for every window:
    + col1: reference contig (chromosome)
    + col2: mean position of the sliding window
    + col3: number of SNPs found in the window (not considering sites with a deletion) 
    + col4: fraction of the window which has a sufficient coverage (min. coverage <= cov <= max.coverage) in every population;
    + col5: average minimum coverage in all populations
    + col6: the pairwise Fst for population 1 and 2

* Fst calculation
```
     Fst = (Pi_total - Pi_within) / Pi_total
     Pi_within = (Pi_population1 + Pi_population2)/ 2
     Pi: (C/(C-1))*(1 - fA ^ 2 - fT ^ 2 -fC ^ 2 - fG ^ 2)
     C: coverage
     fN:  frequency of nucleotide N
     Pi_total: for the total Pi the allele frequencies of the two
     populations are averaged and Pi is calculated as shown above
```

